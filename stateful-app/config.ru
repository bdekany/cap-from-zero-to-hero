require "sinatra/base"
require "json"
require "redis"
require "ostruct"

VCAP_SERVICES = JSON.parse(ENV.fetch("VCAP_SERVICES", JSON.dump({
  redis: [ {
    credentials: {
      hostname: "localhost",
      password: nil,
      port: 6379,
    }
  } ],
})))

if VCAP_SERVICES.has_key?("user-provided")
  REDIS_SERVICES = VCAP_SERVICES.fetch("user-provided").select do |service|
    service.fetch("name").index("redis")
  end
else
  REDIS_SERVICES = VCAP_SERVICES.select { |key, _| key.index("redis") }.values.first
end

REDIS_CREDENTIALS = OpenStruct.new(REDIS_SERVICES.first.fetch("credentials"))

REDIS = Redis.new(
  host: REDIS_CREDENTIALS.hostname || REDIS_CREDENTIALS.host,
  password: REDIS_CREDENTIALS.password,
  port: REDIS_CREDENTIALS.port
)

class Web < Sinatra::Base
  helpers do
    def stop
      ->() {
        pid = Process.pid
        signal = "INT"
        puts "Killing process #{pid} with signal #{signal}"
        Process.kill(signal, pid)
      }
    end
  end

  get "/" do
    @instance = ENV.fetch("CF_INSTANCE_INDEX", 0)
    @addr = ENV.fetch("CF_INSTANCE_ADDR", "127.0.0.1")
    @total_instance_responses = REDIS.incr("total_instance_#{@addr}_responses")
    @total_app_responses = REDIS.incr("total_app_responses")
    erb :index, :layout => :layout
  end

  get "/env.json" do
    content_type "application/json"
    JSON.pretty_generate(ENV.to_h)
  end

  get "/crash" do
    stop.()
    erb :crash, :layout => :layout
  end
end

run Web.new
