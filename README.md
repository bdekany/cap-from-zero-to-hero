# From Zero to Hero

D'un site standalone à un site résilient avec BDD
 * standalone-app/
 * debug-app/
 * stateful-app/

## Scenario
### standalone-app
Site web simple avec une seule instance.
Le but est de faire crasher le site et démonter que le service est indisponible.

```bash
cd standalone-app/
cf push
```

Se connecter au site, cliker sur **crash** recharger la page.

![standalone-app](https://gitlab.com/bdekany/cap-from-zero-to-hero/raw/master/gifs/standalone.gif)

### debug-app
Site web multi-instance qui contient un bug.
Le but est de démontrer les onglets **Log Streams** et **Variables** de Stratos.app

```bash
cd debug-app/
grep instances manifest.yml
cf push
```

Se connecter au site...
Se connecter à Stratos, cliker sur l'application. Dans l'onglet **Log Streams** monter la Stack Trace en rouge.
Dans l'onglet **Variables** ajouter une variable ``FIXED`` = ``True``
Faire un restage
Se connecter au site, cliker sur **crash** recharger la page.

![multiinstance-app](https://gitlab.com/bdekany/cap-from-zero-to-hero/raw/master/gifs/multiinstance.gif)

### stateful-app
Site web multi-instance avec une base de données redis.
Le but est de démonter le système de service.

```bash
cd stateful-app/
cf push --no-start
```
Se connecter à Stratos, cliker sur l'application.  Dans l'onglet **Service** ajouter un service du marketplace : redis 4.0
Faire un restage
Se connecter au site,  recharger la page.


## English

If you need english langage. Clone this repo and translate ``*/views/*.erb`` files.
