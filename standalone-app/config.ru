require "sinatra/base"
require "json"
require "ostruct"


class Web < Sinatra::Base
  helpers do
    def stop
      ->() {
        pid = Process.pid
        signal = "INT"
        puts "Killing process #{pid} with signal #{signal}"
        Process.kill(signal, pid)
      }
    end
  end

  get "/" do
    @instance = ENV.fetch("CF_INSTANCE_INDEX", 0)
    @addr = ENV.fetch("CF_INSTANCE_ADDR", "127.0.0.1")
    erb :index, :layout => :layout
  end

  get "/env.json" do
    content_type "application/json"
    JSON.pretty_generate(ENV.to_h)
  end

  get "/crash" do
    stop.()
    erb :crash, :layout => :layout
  end
end

run Web.new
